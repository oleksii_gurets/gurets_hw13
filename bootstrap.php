<?php
include_once(dirname(__FILE__) . '/classes/PaymentSystem.php');
include_once(dirname(__FILE__) . '/classes/PaymentType.php');
include_once(dirname(__FILE__) . '/classes/MasterCard.php');
include_once(dirname(__FILE__) . '/classes/MasterCardPayment.php');
include_once(dirname(__FILE__) . '/classes/CryptoCurrency.php');
include_once(dirname(__FILE__) . '/classes/CryptoPayment.php');
?>