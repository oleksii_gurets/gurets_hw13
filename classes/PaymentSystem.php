<?php
abstract class PaymentSystem
{
    abstract function getPayment(): PaymentType;

    public function makePayment() {
        $payment = $this->getPayment();
        $payment->payConfirm();
    }
}
?>