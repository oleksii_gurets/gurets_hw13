<?php
class CryptoPayment implements PaymentType
{
    private  $totalPay, $currency, $gatewayAPI;

    public function __construct($totalPay, $currency, $gatewayAPI) {
        $this->totalPay = $totalPay;
        $this->currency = $currency;
        $this->gatewayAPI = $gatewayAPI;
    }

    public function getTotal() {
        return $this->totalPay;
    }

    public function getPaymentInfo() {
        return 'Gateway payment info: ' . '<br>' . 'used cryptocurrency is ' . 
        $this->currency . '<br>' . 'used gatewayAPI is ' . $this->gatewayAPI;
    }

    public function payConfirm() {
        echo 'Payment is confirmed via CryptoWallet system<br>';
        echo 'The total payment is ' . $this->getTotal() . ' tokens<br>';
        echo $this->getPaymentInfo();
    }
}
?>