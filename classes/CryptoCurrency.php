<?php
class CryptoCurrency extends PaymentSystem 
{
    private  $totalPay, $currency, $gatewayAPI;

    public function __construct($totalPay, $currency, $gatewayAPI) {
        $this->totalPay = $totalPay;
        $this->currency = $currency;
        $this->gatewayAPI = $gatewayAPI;
    }

    public function getPayment(): PaymentType {
        return new CryptoPayment($this->totalPay, $this->currency, $this->gatewayAPI);
    }
}
?>