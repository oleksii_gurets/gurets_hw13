<?php
interface PaymentType
{
    public function getTotal();
    public function getPaymentInfo();
    public function payConfirm();
}
?>