<?php
class MasterCard extends PaymentSystem 
{
    private $totalPay, $cardNumber, $cvv;

    public function __construct($totalPay, $cardNumber, $cvv) {
        $this->totalPay = $totalPay;
        $this->cardNumber = $cardNumber;
        $this->cvv = $cvv;
    }

    public function getPayment(): PaymentType {
        return new MasterCardPayment($this->totalPay, $this->cardNumber, $this->cvv);
    }
}
?>