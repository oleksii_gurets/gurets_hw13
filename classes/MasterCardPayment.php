<?php
class MasterCardPayment implements PaymentType
{
    private $totalPay, $cardNumber, $cvv;

    public function __construct($totalPay, $cardNumber, $cvv) {
        $this->totalPay = $totalPay;
        $this->cardNumber = $cardNumber;
        $this->cvv = $cvv;
    }

    public function getTotal() {
        return $this->totalPay;
    }

    public function getPaymentInfo() {
        return 'Card info: ' . '<br>' . 'card number is ' . 
        $this->cardNumber . '<br>' . 'cvv is ' . $this->cvv;
    }

    public function payConfirm() {
        echo 'Payment is confirmed via Master Card system<br>';
        echo 'The total payment is ' . $this->getTotal() . '$<br>';
        echo $this->getPaymentInfo();
    }
}
?>