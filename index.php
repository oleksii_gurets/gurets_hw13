<?php
require_once(dirname(__FILE__) . '/bootstrap.php');

function client(PaymentSystem $payment) {
    $payment->makePayment();
}

client(new MasterCard(315, '0777 7789 56432 XXXX', '1&7'));
echo '</br>';
echo '</br>';
client(new CryptoCurrency(0.345, 'bitcoin', 'blockonomicsAPI'));
?>